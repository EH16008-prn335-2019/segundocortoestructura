#include <stdio.h>

void invertir(int numero){
	printf("%d",numero%10);
	if(numero>10){
		invertir(numero/10);
	}
}

void main(){
	int numero=123;
	printf("Número: %d\n",numero);
	printf("Número invertido: ");
	invertir(numero);
	printf("\n");
}